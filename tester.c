#include "mat_lib.h"
#include <stdio.h>

int main (void){
    float data_a[] = {1, 1, 1, 2, 3, -2, 4, -1, 2};
    Mat * a = mat_alloc(3, 3);
    MAT_ASSIGN(a, data_a);
    MAT_PRINT(a);

    Mat * b = mat_alloc(2, 2);
    mat_trim_row_col(b, a, 1, 1);
    MAT_PRINT(b);

    float x = mat_det(a);

    printf("det a = %f\n", x);
}
