#include "mat_lib.h"
// #include <math.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

data_type rand_data_type(void){
    data_type x = (data_type)rand() / (data_type)RAND_MAX;
    return x;
}


Mat *mat_alloc(size_t rows, size_t cols) {
    assert(rows > 0 && cols > 0);
    Mat *ret = (Mat *)malloc(sizeof(Mat) + sizeof(float) * rows * cols);
    assert(ret != NULL);
    ret->rows = rows;
    ret->cols = cols;

    mat_fill(ret, 0);

    return ret;
}

void mat_free(Mat *m) { free(m); }

Mat *mat_row(Mat *m, size_t row) {
    MAT_CHECK(m);
    Mat *ret = mat_alloc(1, m->cols);
    MAT_CHECK(ret);

    for (size_t j = 0; j < m->cols; j++) {
        MAT_AT(ret, 0, j) = MAT_AT(m, row, j);
    }

    return ret;
}

void mat_print(Mat *m, char *name) {
    MAT_CHECK(m);
    printf("%s = [ \n", name);
    for (size_t i = 0; i < m->rows; i++) {
        printf("    ");
        for (size_t j = 0; j < m->cols; j++) {
            printf("%f", MAT_AT(m, i, j));
            if (j < m->cols - 1) {
                printf(" | ");
            }
        }
        printf("\n");
    }
    printf("]\n");
}

void mat_copy(Mat *dst, Mat *src) {
    MAT_CHECK(dst);
    MAT_CHECK(src);
    assert(dst->rows == src->rows && dst->cols == src->cols);
    for (size_t i = 0; i < dst->rows; i++) {
        for (size_t j = 0; j < dst->cols; j++) {
            MAT_AT(dst, i, j) = MAT_AT(src, i, j);
        }
    }
}

void mat_fill(Mat *a, data_type value) {
    MAT_CHECK(a);

    for (size_t i = 0; i < a->rows; i++) {
        for (size_t j = 0; j < a->cols; j++) {
            MAT_AT(a, i, j) = value;
        }
    }
}

void mat_rand(Mat *a, data_type high) {
    MAT_CHECK(a);
    for (size_t i = 0; i < a->rows; i++) {
        for (size_t j = 0; j < a->cols; j++) {
            MAT_AT(a, i, j) = rand_data_type() * high;
        }
    }
}

void mat_sum(Mat *dst, Mat *a) {
    MAT_CHECK(a);
    MAT_CHECK(dst);
    assert(a->rows == dst->rows);
    assert(a->cols == dst->cols);

    for (size_t i = 0; i < a->rows; i++) {
        for (size_t j = 0; j < a->cols; j++) {
            MAT_AT(dst, i, j) += MAT_AT(a, i, j);
        }
    }
}

void mat_dot(Mat *dst, Mat *a, Mat *b) {
    MAT_CHECK(a);
    MAT_CHECK(b);
    MAT_CHECK(dst);
    assert(b->rows == a->cols);
    size_t n = b->rows;
    assert(dst->rows == a->rows && dst->cols == b->cols);

    for (size_t i = 0; i < dst->rows; i++) {
        for (size_t j = 0; j < dst->cols; j++) {
            MAT_AT(dst, i, j) = (data_type)0;
            for (size_t k = 0; k < n; k++) {
                MAT_AT(dst, i, j) += MAT_AT(a, i, k) * MAT_AT(b, k, j);
            }
        }
    }
}

void mat_activate(Mat *a, data_type (*fn)(data_type)) {
    MAT_CHECK(a);
    for (size_t i = 0; i < a->rows; i++) {
        for (size_t j = 0; j < a->cols; j++) {
            MAT_AT(a, i, j) = fn(MAT_AT(a, i, j));
        }
    }
}


data_type mat_trace(Mat * m){
    MAT_CHECK(m);
    assert(m->rows == m->cols);
    data_type x = (data_type)0;

    for (size_t n = 0; n < m->rows; n++){
        x += MAT_AT(m, n, n);
    }

    return x;
}


void mat_trim_row_col(Mat * dst, Mat * src, size_t row, size_t col){
    MAT_CHECK(src);
    assert(src->rows == src->cols);
    MAT_CHECK(dst);
    assert(dst->rows == dst->cols);
    assert(dst->rows == src->rows - 1);

    size_t ci = 0;
    size_t cj = 0;

    for (size_t i = 0; i < dst->rows; i++){
        if (i == row){ 
            ci = 1;
            // continue;
        }
        for (size_t j = 0; j < dst->cols; j++){
            if (j == col){ 
                cj = 1;
                // continue;
            }
            MAT_AT(dst, i, j) = MAT_AT(src, i + ci, j+ cj);
        }

        cj = 0;

    }
}

data_type mat_det(Mat* m){
    MAT_CHECK(m);
    assert(m->cols == m->rows);
    assert(m->cols > 1);
    // The Laplace expansion will take place allong the first row (i = 0).
    data_type acc = 0;
    if (m->cols == 2){
        // This the end case that allows the reccursion to end
        acc += MAT_AT(m, 0, 0) * MAT_AT(m, 1, 1) - MAT_AT(m, 1, 0) * MAT_AT(m, 0, 1);
    } else {
        for (size_t j = 0; j < m->cols; j++){
            data_type sign = j % 2 == 0 ? (data_type)(1) : (data_type)(-1);
            Mat * sub_mat = mat_alloc(m->rows - 1, m->cols - 1);
            mat_trim_row_col(sub_mat, m, 0, j);
            acc += mat_det(sub_mat) * sign * MAT_AT(m, 0, j);
            mat_free(sub_mat);

        }
    }
    return acc;

}

