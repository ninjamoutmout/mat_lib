# Mat_lib.h

A simple linear algebra library made in C. Mostly for my own use and education.

## Usage

TODO

## Features

The main type is a "Mat" : 
```c
typedef struct Mat{
    size_t rows;
    size_t cols;
    data_type data[];
} Mat;
```
with ``data_type`` beeing defined using the ``MAT_LIB_DT`` macro. If it's left undefined it will default to float (f32). Setting it to something weird will most probably result in weird behaviours.

It must be noted that because of the variable size member of the ``Mat`` it must always be handled through pointers.
It must also be noted that only a few functiuns are allowed to perform memory manipulations (.i.e ``mat_alloc``, ``mat_free``, ``mat_row`` and ``mat_col``).

The ``mat_det`` function uses Laplace expansion reccursively until it hits 2x2 but this might not be most efficient way there to compute the determinant (the whole operation is O(n!)).

- [x] Basic operations on matrix
  - [x] Sum
  - [x] Multiplication
  - [x] Activation by a function
  - [x] Trace
  - [x] Determinant

- [ ] More complicated operations on matrix
  - [ ] Inversing
  - [ ] Diagonalization
  - [ ] Eigen values
