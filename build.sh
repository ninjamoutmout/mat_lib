#!/bin/sh
# FLAGS="-Wall -Wextra -fsanitize=address,undefined -Iinclude"
FLAGS="-Wall -Wextra -Iinclude"
LIBS="-lm"
OUT="build"



gcc $FLAGS -g -c -o $OUT/mat_lib.o mat_lib.c
gcc $FLAGS -g -o $OUT/tester $OUT/mat_lib.o tester.c $LIBS
