#ifndef MAT_LIB_H
#define MAT_LIB_H

#include <stdlib.h>

#define MAT_AT(m, i, j) (m)->data[(i)*(m)->cols + (j)] // unsafe macro to easily get the i,j coefficient. 
#define MAT_CHECK(m) do { assert((m) != NULL && (m)->cols != 0 && (m)->rows !=0); } while(0) // unsafe macro that verifies that the provided matrices are valid. Used almost in every functiun.
#define MAT_PRINT(m) do { mat_print((m), (#m)); } while(0) // unsafe marco to print a matrice along with it's variable name.

// unsafe marco to quickly assign a list of numbers to a matrice. It doesn't check for dimmensions.
#define MAT_ASSIGN(m, list) do {                                    \
    for (size_t P = 0; P < sizeof((list))/sizeof((list)[0]); P++){  \
        (m)->data[P] = (list)[P];                                   \
    }                                                               \
} while (0)                                                         \

#ifndef MAT_LIB_DT
typedef float data_type;
#else 
typedef MAT_LIB_DT data_type;
#endif // MAT_LIB_DT


typedef struct Mat{
    size_t rows;
    size_t cols;
    data_type data[];
} Mat;

data_type rand_data_type(void); // Returns a number of type data_type between zero and one.

// To avoid mistakes only these functiuns perform memory managment.
Mat * mat_alloc(size_t rows, size_t cols); // A simple wrapper arround malloc. Matrices created this way are filled with zeroes.
void mat_free(Mat * m); // A simple wrapper arround free.
Mat * mat_row(Mat * m, size_t row); // A way to create a matrix from anothe matrix row. This functiun perform a memory allocation.
// TODO : mat_col 

void mat_print(Mat * m, char * name);
void mat_copy(Mat * dst, Mat * src);

void mat_fill(Mat * a, data_type value); // Fills a given matrice with a given value.
void mat_rand(Mat * a, data_type high); // Randomize a given matrice with a given high value.

void mat_sum(Mat * dst, Mat * a); // Computes dst + A and stores it in dst.
void mat_dot(Mat * dst, Mat * a, Mat * b); // Computes A x B and stores it in dst.
void mat_activate(Mat * a, data_type (*fn)(float)); // Passes all of the value of a given matrix through the same functiun and stores it back.

data_type mat_trace(Mat * m); // Computes the trace of a given matrix.
void mat_trim_row_col(Mat * dst, Mat * src, size_t row, size_t col); // Takes two matrices dst (n*n) and src (n-1 * n-1) and copies the data from src to dst but ommits the col-th collumn and the row-th row.
data_type mat_det(Mat * m); // Computes the determinant of a given matrix by using laplace methode thus beeing O(n!).
// TODO : mat_inverse
// TODO : mat_diagonalize

#endif // MAT_LIB_H
